fun_login(){
        if grep -q -w "$username $password" "$txtUser"
        then
                echo "$tanggal $waktu LOGIN:INFO User $username logged in" >> $txtLog
		echo "Log in sukses"
                echo "Pilih perintah dl | att"
                read command
                if [[ $command == att ]]
		then
                        att
                elif [[ $command == dl ]]
		then
                        dl
                else
                        echo "Perintah tidak ditemukan"
                fi
        else
                msgFail="Failed login attemp on user $username"
                echo $msgFail
                echo $tanggal $waktu LOGIN:ERROR $msgFail >> $txtLog
        fi
}

att(){
	#username jika berhasil terdapat pada arg ke 5 atau 9
	awk -v user="$username" 'BEGIN {N=0} $5 == user || $9 == user {N++} END {print "Terdapat " N-1 "kali percobaan login dari "user}' $txtLog 
}

cnt=0
dl(){
	echo "Masukkan angka: "
	read n
	if [[ ! -f "$fileName.zip" ]]
	then
		mkdir $fileName #bikin folder dulu
		dl_foto
	else
		fun_unzip #langsung unzip
	fi
}

fun_unzip(){
	unzip -P $password $fileName.zip
	rm $fileName.zip
	cnt=$(find $fileName -type f | wc -l)
	fun_dl_foto
}



dl_foto(){
	for(( i=$cnt+1; i<=$n+$cnt; i++ ));do
		wget https://loremflickr.com/320/240 -O $fileName/PIC_$i.jpg
	done
	zip --password $password -r $fileName.zip $fileName
	rm -rf $fileName 
}

echo "Login"
echo "Username:"
read username
echo "Password:"
read -s password

txtUser=~/users/user.txt
txtLog=~/soal-shift-sisop-modul-1-e12-2022/log.txt
tanggal=$(date +%D)
waktu=$(date +%T)
fileName=$(date +%Y-%m-%d)_$username

#check password, this time kita pake ga pake forloop :)
if [[ ${#password} -lt 8 ]]
then
	echo "Password minimal 8 karakter"
elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
then
	echo "Password harus terdiri dari setidaknya sebuah angka, huruf kapital, dan huruf kecil"
elif [[ $password == $username ]]
then
	echo "Password tidak boleh sama dengan username"
elif [[ "$password" =~ [^a-zA-Z0-9] ]]
then
	echo "Password tidak boleh berisi simbol" 
else
	fun_login
fi

