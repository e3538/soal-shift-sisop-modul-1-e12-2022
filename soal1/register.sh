#!/bin/bash 
echo "Register Account"
echo "Username:"
read username
echo "Password:"
read -s password

txtUser=~/users/user.txt
txtLog=~/soal-shift-sisop-modul-1-e12-2022/log.txt
numberReg="[0-9]"
capReg="[A-Z]"
smlReg="[a-z]"
alpNum="\W"
cntNum=0
cntSml=0
cntBig=0
tanggal=$(date +%D)
waktu=$(date +%T)

if grep -q $username "$txtUser"
then
	errMsg="User already exist"
	echo $errMsg
	echo $tanggal $waktu REGISTER:ERROR $errMsg >> $txtLog
	exit 0
elif [[ $username == $password ]]; then
	echo "Password tidak boleh sama dengan username"
	exit 0
#Check panjang password
elif [ ${#password} -lt 8 ];
	then
	echo "Password minimal 8 karakter"
	exit 0
fi

#Check password
for((i=0 ; i< ${#password};i++)); do
	#Check angka
	if [[ "${password:$i:1}" =~ $numberReg ]]; then
		((cntNum = cntNum + 1))
	fi
	#Check huruf kecil
	if [[ "${password:$i:1}" =~ $smlReg ]]; then
		((cntSml = cntSml + 1))
	fi
	#Check huruf kapital
	if [[ "${password:$i:1}" =~ $capReg ]]; then
		((cntBig = cntBig + 1))
	fi
	if [[ "${password:$i:1}" =~ $alpNum ]]; then
		echo "Password tidak boleh berisi simbol"
		exit 0
	fi
done

if [[ $cntNum -lt 1 || $cntSml -lt 1 || $cntBig -lt 1 ]]
	then
		echo "Password harus terdiri dari setidaknya  sebuah angka, huruf kapital, dan huruf kecil"
else
	echo "Selamat akun anda berhasil dibuat"
	echo $tanggal $waktu $username $password >> $txtUser
	echo $tanggal $waktu REGISTER:INFO User $username registered succesfully >> $txtLog
fi
