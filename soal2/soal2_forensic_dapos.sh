#!/bin/bash
logFile=~/log_website_daffainfo.log
locFolder=~/forensic_log_website_daffainfo_log
echo "[Program berhasil berjalan]"

if [[ $(id -u) -ne 0 ]]
then
	    echo "[Mohon jalankan sebagai superuser/root!]"
	    exit 1
fi

if [[ ! -d "$locFolder" ]]
then
    	echo "Membuatkan folder…"
    	mkdir $locFolder
else
    	rm -rf $locFolder
    	mkdir $locFolder
fi

cat $logFile | awk -F: 'NR>1 {gsub(/"/, "", $3) arr[$3]++}
        END {
            for ( i in arr ){
                hour++
                avg+=arr[i]
            }
            avg = avg/hour
            printf "Rata-rata serangan adalah sebanyak %.2f requests per jam\n", avg
        }' >> $locFolder/ratarata.txt
maxAccess=0
ind=0
cat $logFile | awk -F: 'NR>1 {gsub(/"/, "", $1) arr[$1]++}
        END {
            for ( ip in arr ){
                if(arr[ip] > maxAccess){
                    ind=ip
                    maxAccess=arr[ip]
                }
            }
            print "IP yang paling banyak mengakses server adalah: " ind "\nCounter: " maxAccess " requests\n"
        }' >> $locFolder/result.txt

cat $logFile | awk -F: ' /curl/ {counter++}
        END {
            print "Ada " counter " requests yang menggunakan curl sebagai user-agent\n"
        }' >> $locFolder/result.txt

cat $logFile | awk -F: '/2022:02/ {arr[$1]++}
        END {
            for ( ip in arr ){
                print ip "Jam 2 pagi\n"
            }
        }' >> $locFolder/result.txt
exit
