|     NRP    |     Nama    |
| :--------- |:--------    |
| 5025201076 | Raul Ilma Rajasa |
| 5025201163 | Muhammad Afdal Abdallah |
| 5025201202 | Aiffah Kiysa Waafi |


# soal shift sisop modul 1 E12 2022

# Soal 1
Tujuan utama dari soal 1 yaitu untuk membuat sistem register dan login. Setiap kegiatan login atau register
akan ditampilkan pada log sesuai dengan status dari kegiatan. Ketika berhasil login, user memiliki dua pilihan,
yaitu att atau dl. Pilihan att akan menampilkan berapa banyak user tersebut telah mencoba untuk login
sejak awal hingga user berhasil login. Pilihan dl akan mendownload image sebanyak N kali, kemudian image tersebut disimpan
di dalam zip.

## Register 
Setiap user yang berhasil register akan disimpan pada /users/user.txt . 
User akan menginputkan username dan password. Terdapat beberapa persyaratan untuk pembuatan password, yaitu:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumerical
- Tidak boleh sama dengan username

Menggunakan -s pada read untuk membaca input secara hidden
```
echo "Register Account"
echo "Username:"
read username
echo "Password:"
read -s password
```

Sistem register pada biasanya akan mengecek apabila username sudah ada atau belum, maka langkah pertama setelah input username dan password yaitu untuk mengecek apakah username sudah ada di user.txt . Pengecekan username dilakukan dengan menggunakan fungsi grep dimana grep akan mencari pada file sesuai dengan patern yang diinput. Pada kasus ini kita mencari username pada file user.txt .
```
if grep -q $username "$txtUser"
then
	errMsg="User already exist"
	echo $errMsg
	echo $tanggal $waktu REGISTER:ERROR $errMsg >> $txtLog
	exit 0
```

Jika username tidak ditemukan, maka proses selanjutnya yaitu melakukan pengecekan untuk password. 
- Mengecek minimal 8 karakter dengan mengecek apakah panjang karakter lebih sedikit dari (-lt) 8 atau tidak. Jika iya, maka proses dihentikan dan mengeluarkan message bahwa password minimal 8 karakter, jika tidak lebih sedikit, maka lanjutkan untuk syarat selanjutnya.

```
elif [ ${#password} -lt 8 ];
	then
	echo "Password minimal 8 karakter"
	exit 0
```

- Mengecek kalau password tidak sama dengan username dengan mengecek if password -eq username, jika iya maka keluarkan error message, jika tidak maka lanjut untuk syarat selanjutnya

```
elif [[ $username == $password ]]; then
	echo "Password tidak boleh sama dengan username"
	exit 0
```

Untuk mengecek jika password memiliki minimal 1 huruf kapital, 1 huruf kecil, dan alphanumeric dengan mengiterasi setiap character pada password dan dilakukan pengecekan
- Mengecek setidaknya 1 huruf kapital dan huruf kecil dengan mentracking banyaknya huruf kecil dan huruf kapital, jika pada akhir iterasi didapatkan huruf kecil dan huruf kapital lebih dari satu, maka lanjutkan.
- Mengecek alphanumeric dengan melakukan perbandingan dengan "\W", dimana "\W" merupakan regex untuk kata yang tidak alphanumerical. Jika sama, maka keluarkan error message, jika tidak maka lanjutkan program.

```
for((i=0 ; i< ${#password};i++)); do
	#Check angka
	if [[ "${password:$i:1}" =~ $numberReg ]]; then
		((cntNum = cntNum + 1))
	fi
	#Check huruf kecil
	if [[ "${password:$i:1}" =~ $smlReg ]]; then
		((cntSml = cntSml + 1))
	fi
	#Check huruf kapital
	if [[ "${password:$i:1}" =~ $capReg ]]; then
		((cntBig = cntBig + 1))
	fi
	if [[ "${password:$i:1}" =~ $alpNum ]]; then
		echo "Password tidak boleh berisi simbol"
		exit 0
	fi
done
```

Ketika semua persyaratan password dan username terpenuhi, maka print pesan log di log.txt dengan format MM/DD/YY HH:MM:SS  REGISTER:
INFO User **USERNAME** registered successfully. Dan disimpan pada user.txt dengan format MM/DD/YY HH:MM:SS **Username** **Password**

```
if [[ $cntNum -lt 1 || $cntSml -lt 1 || $cntBig -lt 1 ]]
	then
		echo "Password harus terdiri dari setidaknya  sebuah angka, huruf kapital, dan huruf kecil"
else
	echo "Selamat akun anda berhasil dibuat"
	echo $tanggal $waktu $username $password >> $txtUser
	echo $tanggal $waktu REGISTER:INFO User $username registered succesfully >> $txtLog
fi
```

Proses Register selesai.

## Login
Sistem login memiliki persyaratan yang sama untuk password, namun pada main.sh, sudah diberlakukan penggunaan regex untuk mengetahui apabila password setidaknya memiliki 1 angka, 1 huruf kapital, 1 huruf kecil, dan alphanumerical

```
if [[ ${#password} -lt 8 ]]
then
	echo "Password minimal 8 karakter"
elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
then
	echo "Password harus terdiri dari setidaknya sebuah angka, huruf kapital, dan huruf kecil"
elif [[ $password == $username ]]
then
	echo "Password tidak boleh sama dengan username"
elif [[ "$password" =~ [^a-zA-Z0-9] ]]
then
	echo "Password tidak boleh berisi simbol" 
else
	fun_login
fi
```

Ketika berhasil mengisi password, maka program akan menjalankan fungsi fun_login, dimana fungsi fun_login berisi pengecekan akun pada user.txt dan penentuan langkah selanjutnya. Jika ditemukan usernam dan password yang sesuai, maka program akan memberikan dua pilihan, yaitu att atau dl N. 
```
fun_login(){
        if grep -q -w "$username $password" "$txtUser"
        then
                echo "$tanggal $waktu LOGIN:INFO User $username logged in" >> $txtLog
		echo "Log in sukses"
                echo "Pilih perintah dl | att"
                read command
                if [[ $command == att ]]
		then
                        att
                elif [[ $command == dl ]]
		then
                        dl
                else
                        echo "Perintah tidak ditemukan"
                fi
        else
                msgFail="Failed login attemp on user $username"
                echo $msgFail
                echo $tanggal $waktu LOGIN:ERROR $msgFail >> $txtLog
        fi
}
```

Ketika memilih att, maka program menjalankan fungsi att, yaitu untuk mengetahui log info dari username tersebut. Log info yang diinginkan yaitu login attempt dari username tersebut. Login attempt username tersebut dapat ditemukan pada argumen ke 5 atau 9 pada log.txt . Banyaknya attempt dikurangi 1, karena ketika mengecek argumen 5 atau 9, maka log register akan terhitung, karena register hanya sekali, maka banyak attempt dikurangi 1.

```
att(){
	#username jika berhasil terdapat pada arg ke 5 atau 9
	awk -v user="$username" 'BEGIN {N=0} $5 == user || $9 == user {N++} END {print "Terdapat " N-1 "kali percobaan login dari "user}' $txtLog 
}
```

Pilihan dl merupakan perintah untuk mendownload foto pada website sebanyak N kali. Foto yang di download disimpan dalam sebuah .zip dan diberi password sesuai dengan akun yang sedang mengakses saat itu. Penamaan .zip berdasarkan tanggal, apabila terdapat nama .zip yang sama, maka .zip akan dibuka, kemudian foto yang baru didownload akan digabungkan ke dalam .zip yang lama. Oleh sebab itu, kita perlu mengecek apakah sudah ada file .zip tersebut, dan melakukan langkah selanjutnya berdasarkan hasil pengecekan.

```
dl(){
	echo "Masukkan angka: "
	read n
	if [[ ! -f "$fileName.zip" ]]
	then
		mkdir $fileName #bikin folder dulu
		dl_foto
	else
		fun_unzip #langsung unzip
	fi
}
```

Jika sudah ada, maka akan di unzip, kemudian baru mendownload, jika belum ada, maka akan langsung mendownload foto.
```
fun_unzip(){
	unzip -P $password $fileName.zip
	rm $fileName.zip
	cnt=$(find $fileName -type f | wc -l)
	fun_dl_foto
}



dl_foto(){
	for(( i=$cnt+1; i<=$n+$cnt; i++ ));do
		wget https://loremflickr.com/320/240 -O $fileName/PIC_$i.jpg
	done
	zip --password $password -r $fileName.zip $fileName
	rm -rf $fileName 
}
```
Program main.sh selesai.

## Dokumentasi Program no 1
Proses Registrasi


![image-1.png](https://drive.google.com/uc?export=view&id=1AJKFwIj3MlnDh-0E_ouhKtVRPRNjNxjM)


File user.txt setelah registrasi akun


![image-2.png](https://drive.google.com/uc?export=view&id=1mYEfzJLiCRqejbQ7ppC-ZmOAumWNPgbY)


Ketika registrasi dengan password berisi simbol


![image-3.png](https://drive.google.com/uc?export=view&id=1G95GQhrl4xmxEsq3weRzXD9sEtiGn871)


Login berhasil


![image-4.png](https://drive.google.com/uc?export=view&id=1bALaXbDnQJ70qNFSDA89HNhzSSFsKdZ3)


File log.txt


![image-5.png](https://drive.google.com/uc?export=view&id=1A4_I96hOnQW50ej3dLZBWeSY_tRBkhH9)


Memilih att


![image-6.png](https://drive.google.com/uc?export=view&id=1SHtEhOBrRie_IVUB-CXSg-LzdgWvBr_L)


Memilih dl dengan N=5


![image-7.png](https://drive.google.com/uc?export=view&id=1tc02uDrBDXd6vVlTHUUT7m8hZZHJhEgJ)


File .zip


![image-8.png](https://drive.google.com/uc?export=view&id=13Czi6M2ieSm1iWkKl6dqBNLqdETg6RrE)


# Soal 2
Soal ini memiliki tujuan untuk membuat suatu program yang dapat melakukan analisis forensik melalui scripting awk. Dengan analogi yang ada pada soal berupa kasus website (https://daffa.info) yang dihack oleh seseorang, kita diminta untuk menganalisis rerata request per jam yang dikirimkan penyerang ke website, mengidentifikasi IP yang paling banyak melakukan request ke server, jumlah request dengan user-agent curl, serta daftar IP yang mengakses website pada jam 2 pagi.

## Pengecekan Access
Pertama-tama, dilakukan pengecekan akses executable pada program shell scripting. Misalnya, pada kasus ketika kita mengeksekusi program soal2_forensic_dapos.sh dengan perintah shell "bash soal2_forensic_dapos.sh,"
```
bash soal2_forensic_dapos.sh
```

maka otorisasi terhadap program menjadi terbatas. Oleh karena itu, dianjurkan untuk menggunakan otorisasi akses dari superuser atau root untuk menjalankan program dengan lebih optimal, "sudo bash soal2_forensic_dapos.sh."
```
su
sudo bash soal2_forensi_dapos.sh
```

## Validasi Kondisi Log File
Dalam menyelesaikan soal ini, diperlukan file log untuk mencatat proses selama program berjalan di sesi tertentu.
```
logFile=/home/aiffah/log_website_daffainfo.log
```

Jika file dalam direktori yang diberikan (dalam kasus ini: /home/aiffah/log_website_daffainfo.log) belum tersedia, maka file log harus dibuat terlebih dahulu. Sebaliknya, jika file sudah tersedia, maka sesi log tersebut akan dihapus dan diganti dengan sesi baru, sehingga proses yang dilakukan adalah menghapus file log sebelumnya menggunakan rm dan membuat file log yang baru menggunakan mkdir. Metode yang digunakan adalah dengan analisis shell script: -f "<lokasi file target>" untuk mendeteksi adanya file atau tidak pada lokasi yang diberikan. Untuk membuat file log, hanya perlu dilakukan proses printing pada file log, yaitu: printf "Log File Soal 2 E12\n" > $logFile

## Validasi Kondisi Folder
```
locFolder=/home/aiffah/forensic_log_website_daffainfo_log
```
Untuk memvalidasi kondisi folder, tentu saja diperlukan untuk membaca apakah direktori tersebut hadir dalam storage atau tidak. Hal ini memiliki tujuan yang sama dengan tahap sebelumnya. Akan tetapi, metode yang digunakan adalah dengan analisis shell script: -d "<lokasi folder>" untuk mendeteksi adanya file atau tidak pada lokasi yang diberikan. 
```
if [[ ! -d "$locFolder" ]]
then
    	echo "Membuatkan folder…"
    	mkdir $locFolder
else
    	rm -rf $locFolder   
    	mkdir $locFolder    
fi

```

Untuk membuat folder log tersebut, hanya perlu shell script berupa: mkdir <lokasi folder target>

## Rata-Rata Serangan (x Request per Jam)
Untuk menghitung rerata serangan, kami menggunakan AWK looping melalui array yang telah diolah melalui gsub (global substitution) untuk mensubstitusikan input IP yang ada menjadi format IP yang diinginkan. Setelah itu dilakukan, program akan melakukan loop dalam array yang telah diolah dengan variabel "i" yang menotasikan setiap nilai dalam array tersebut. Dalam loop tersebut, akan dilakukan increment pada variabel hour dan addition pada nilai rerata (avg) sebanyak nilai array pada index ke-i. Setelah loop tersebut selesai, baru dilakukan averaging dengan membagi variabel avg dengan jumlah jam (hour) yang telah diproses dari loop. Kemudian hasil berupa nilai dari avg akan diprint dan dimasukkan ke dalam file rata-rata.txt.
```
cat $logFile | awk -F: 'NR>1 {gsub(/"/, "", $3) arr[$3]++} 
        END {
            for ( i in arr ){
                hour++
                avg+=arr[i]
            }
            avg = avg/hour
            printf "Rata-rata serangan adalah sebanyak %.2f requests per jam\n", avg
        }' >> $locFolder/ratarata.txt
```

## IP yang Paling Banyak Mengakses Server
Untuk mencari IP yang paling banyak mengakses server, kami menggunakan AWK looping dengan langkah yang sama seperti sebelumnya. Kemudian dicari IP dan jumlah request terbanyak yang ada dalam array, variabel "ind" merupakan IP yang paling banyak mengakses server dan variabel "max" adalah jumlah request terbanyak. Output IP dan nilai max akan diprint serta dimasukkan ke dalam file result.txt.
```
maxAccess=0
ind=0
cat $logFile | awk -F: 'NR>1 {gsub(/"/, "", $1) arr[$1]++}
        END {
            for ( ip in arr ){
                if(arr[ip] > maxAccess){
                    ind=ip
                    maxAccess=arr[ip]
                }
            }
            print "IP yang paling banyak mengakses server adalah: " ind "\nCounter: " maxAccess " requests\n"
        }' >> $locFolder/result.txt

```

## Requests yang menggunakan user-agent curl
Untuk menghitung banyak request yang menggunakan curl sebagai user-agent, kami menggunakan AWK dimana setiap ditemukan curl sebagai user agent maka variabel n terus bertambah. Hasil n merupakan banyaknya request yang menggunakan curl sebagai user agent akan diprint dan dimasukkan ke dalam file result.txt
```
cat $logFile | awk -F: ' /curl/ {counter++}
        END {
            print "Ada " counter " requests yang menggunakan curl sebagai user-agent\n"
        }' >> $locFolder/result.txt

```

## IP yang mengakses website pada jam 2 pagi
Untuk mencari IP yang mengakses website pada jam 2 pagi, kami juga menggunakan AWK dimana setiap ditemukan 2022:02 maka arr[i] terus bertambah. Kemudian menggunakan looping akan diprint "Jam 2 Pagi" sebanyak index-i dalam array, dan dimasukkan ke dalam file result.txt.
```
cat $logFile | awk -F: '/2022:02/ {arr[$1]++}
        END {
            for ( ip in arr ){
                print ip "Jam 2 pagi\n"
            }
        }' >> $locFolder/result.txt

```

## Hasil soal 2

Pada home akan ada sebuah folder bernama forensic_log_website_daffainfo_log.

![image-9.png](https://drive.google.com/uc?export=view&id=10FrS_ki660JmFhUUoZ5kZ5QnsK29tkCL)

Dalam folder tersebut terdapat 2 file yaitu rata-rata.txt dan result.txt.

![image-10.png](https://drive.google.com/uc?export=view&id=1EUoDF5BX9XAQiDcQUsOOIv_a39XGUtug)

File rata-rata.txt berisikan jawaban rata-rata request per jam.

![image-11.png](https://drive.google.com/uc?export=view&id=1NOAesrzgK7W0C3_7zdrI2ALKYfZcSqtF)

File result.txt berisikan jawaban IP yang banyak mengakses server dan jumlah aksesnya, jumlah request user-agent curl, dan IP yang mengakses pada jam 2 pagi.

![image-12.png](https://drive.google.com/uc?export=view&id=1w_yKAm_a30PtpEf754jPzWSvEL4dqKA-)


# Soal 3
Soal ini bertujuan untuk membuat suatu program monitoring resource yaitu monitoring ram dan monitoring size suatu directory pada komputer.
# Mencatat Metrics dengan Format metrics_{YmdHms}.log
Untuk menyelesaikan problem ini, digunakan kode sebagai berikut:
```
#!/bin/bash

fd="$(date -d '1 hour ago' +'%Y%m%d%H')"
d=$(date +"%Y%m%d%H%M%S")

loc=/home/raul/log/$fd

if [[ ! -d "$loc" ]]
then
    	mkdir $loc
fi

echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$loc/metrics_$d.log"
free -m /home/raul | awk '/[0-9]+/{for (i=2; i<NF; i++) printf $i ","; print $NF}' ORS="," >> "$loc/metrics_$d.log"
du -sh /home/raul | awk '/[0-9]+/{print $2","$1}' >> "$loc/metrics_$d.log"

# crontab b: * * * * * /home/raul/minute_log.sh
# crontab c: 0 * * * * /home/raul/aggregate_minutes_to_hourly_log.sh
# d: chown "raul" "/home/raul/log/$fd"
```
Dengan pendeklarasian variabel "fd" untuk memberikan penamaan pada lokasi folder tempat file log akan disetor dengan rumus (jam sekarang - 1 --> kurang satu jam dari sekarang).
```
fd="$(date -d '1 hour ago' +'%Y%m%d%H')"
```
Contoh:
Dalam kasus ketika kita ingin mencatat metrics pada jam sekarang (05:08) pada tanggal 5 Maret 2022, maka nama folder yang akan dibuat adalah "2022030504" dengan lokasi direktori file log nantinya di "/home/raul/log/2022030504."
Sedangkan, untuk variabel d akan digunakan untuk membuat format penamaan file log dari metrics sesuai kriteria dari soal.
```
d=$(date +"%Y%m%d%H%M%S")
```
Variabel loc digunakan untuk menyimpan direktori penyimpanan dari file log sesuai penjelasan pada contoh sebelumnya.
```
loc=/home/raul/log/$fd
```
Lalu, akan dilakukan pengecekan terhadap eksistensi dari direktori loc tersebut.
```
if [[ ! -d "$loc" ]]
then
    	mkdir $loc
fi
```
Jika direktori loc belum tersedia di local storage, maka direktori loc tersebut akan langsung dibuat.

Lalu, tinggal dilakukan pencetakan output sesuai dengan constraint yang diberikan pada soal.
1. Head (1st line)
```
echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$loc/metrics_$d.log"
```
2. Mem & Swap Statistics
```
free -m /home/raul | awk '/[0-9]+/{for (i=2; i<NF; i++) printf $i ","; print $NF}' ORS="," >> "$loc/metrics_$d.log"
```
Dengan memodifikasi Field Separator (FS) dan Record Separator (RS) yang ada ketika akan memasukkan metrics ke file log.
3. Path & Path-Size
```
du -sh /home/raul | awk '/[0-9]+/{print $2","$1}' >> "$loc/metrics_$d.log"
```

# Mencatat Metrics secara Otomatis setiap Menit
Untuk mencatat metrics file log dalam setiap menitnya, perlu menggunakan crontab.
Pada terminal, jalankan
```
crontab -e
```
Editor crontab pun akan terbuka, langsung isi dengan konfigurasi cron yang membuat program bash script dari minute_log.sh berjalan setiap menitnya dengan path dari file minute_log.sh. Dalam kasus ini minute_log.sh berletak pada "/home/raul/minute_log.sh"
Konfigurasi cron eksekusi program .sh setiap menit:
```
* * * * * /home/raul/minute_log.sh
```

# Agregasi File Log ke Satuan Jam
Solusi:
```
#!/bin/bash
d="$(date -d '1 hour ago' +'%Y%m%d%H')"
if ! [[ -d /home/raul/log/$d ]]
	then
    	mkdir /home/raul/log/$d
fi
files=($(awk '{print}' <<< `ls -r "/home/raul/log/$d"`))

mtavg=0
muavg=0
mfavg=0
msavg=0
mbavg=0
maavg=0
stavg=0
suavg=0
sfavg=0
psavg=0
id=0

for i in ${files[@]}
do
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $1}'))
	mt[$id]=$val
	mtavg=$(($mtavg+${mt[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $2}'))
	mu[$id]=$val
	muavg=$(($muavg+${mu[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $3}'))
	mf[$id]=$val
	mfavg=${mf[$id]}
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $4}'))
	ms[$id]=$val
	msavg=$(($msavg+${ms[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $5}'))
	mb[$id]=$val
	mbavg=$(($mbavg+${mb[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $6}'))
	ma[$id]=$val
	maavg=$(($maavg+${mb[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $7}'))
	st[$id]=$val
	stavg=$(($stavg+${st[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $8}'))
	su[$id]=$val
	suavg=$((${su[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $9}'))
	sf[$id]=$val
	sfavg=$(($sfavg+${sf[$id]}))
	path=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $10}'))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="[M,]" 'NR!=1 {print $11}'))
	path_size[$id]=$val
	psavg=$(($psavg+${path_size[$id]}))
	((id++))
done

mt_sorted=($(for i in "${mt[@]}"; do echo "$i"; done | sort -n))
mu_sorted=($(for i in "${mu[@]}"; do echo "$i"; done | sort -n))
mf_sorted=($(for i in "${mf[@]}"; do echo "$i"; done | sort -n))
ms_sorted=($(for i in "${ms[@]}"; do echo "$i"; done | sort -n))
mb_sorted=($(for i in "${mb[@]}"; do echo "$i"; done | sort -n))
ma_sorted=($(for i in "${ma[@]}"; do echo "$i"; done | sort -n))
st_sorted=($(for i in "${st[@]}"; do echo "$i"; done | sort -n))
su_sorted=($(for i in "${su[@]}"; do echo "$i"; done | sort -n))
sf_sorted=($(for i in "${sf[@]}"; do echo "$i"; done | sort -n))
path_size_sorted=($(for i in "${path_size[@]}"; do echo "$i"; done | sort -n))

mt_avg=$((mtavg/id))
mu_avg=$((muavg/id))
mf_avg=$((mfavg/id))
ms_avg=$((msavg/id))
mb_avg=$((mbavg/id))
ma_avg=$((maavg/id))
st_avg=$((stavg/id))
su_avg=$((suavg/id))
sf_avg=$((sfavg/id))
path_size_avg=$((psavg/id))
((id--))
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "/home/raul/log/$d/metrics_agg_$d.log"
echo "minimum,${mt_sorted[0]},${mu_sorted[0]},${mf_sorted[0]},${ms_sorted[0]},${mb_sorted[0]},${ma_sorted[0]},${st_sorted[0]},${su_sorted[0]},${sf_sorted[9]},$path,${path_size_sorted[0]}M" >> "/home/raul/log/$d/metrics_agg_$d.log"
echo "maximum,${mt_sorted[$id]},${mu_sorted[$id]},${mf_sorted[$id]},${ms_sorted[$id]},${mb_sorted[$id]},${ma_sorted[$id]},${st_sorted[$id]},${su_sorted[$id]},${sf_sorted[$id]},$path,${path_size_sorted[$id]}M" >> "/home/raul/log/$d/metrics_agg_$d.log"
echo "average,$mt_avg,$mu_avg,$mf_avg,$ms_avg,$mb_avg,$ma_avg,$st_avg,$su_avg,$sf_avg,$path,$path_size_avg""M" >> "/home/raul/log/$d/metrics_agg_$d.log"

# chown "raul" "/home/raul/log/$d/metrics_agg_$d.log"
```
Pertama-tama, dilakukan pendeklarasian variabel, seperti "d" yang membuat pemformatan nama lokasi di direktori loc. Kemudian, mtavg. muavg, ..., psavg yang bernilai 0 digunakan untuk menyetor nilai awal dari beberapa indikator mem & swap yang kita butuhkan untuk mengklasifikasikannya pada soal ini. Sedangkan, variabel files berguna untuk membaca file-file log yang ada pada direktori loc dan "id" berguna untuk melakukan incremental pada jumlah file log yang berhasil terbaca oleh files.
```
files=($(awk '{print}' <<< `ls -r "/home/raul/log/$d"`))

mtavg=0
muavg=0
mfavg=0
msavg=0
mbavg=0
maavg=0
stavg=0
suavg=0
sfavg=0
psavg=0
id=0
```
Selanjutnya, akan dilakukan looping untuk setiap file log yang berhasil terbaca oleh files. Untuk metode read/bacanya adalah dengan menggunakan cat pada direktori file yang telah dispesifikasikan. Isi dari loop tersebut adalah menyetor data-data yang telah terbaca dari file log ke dalam variabel-variabel yang telah kita deklarasikan sebelumnya. Dengan menggunakan FS (Field Separator) dan NR (Number of Records) dari awk untuk mengolah teks yang ada pada file log metricsnya. Lalu, akan dilakukan penyetoran pula dari masing-masing indikator statistik mem & swap melalui array, seperti mt[id], mu[id], ..., dan ps[id]. Kemudian, akan dilakukan iterasi pada variabel id.
```
for i in ${files[@]}
do
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $1}'))
	mt[$id]=$val
	mtavg=$(($mtavg+${mt[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $2}'))
	mu[$id]=$val
	muavg=$(($muavg+${mu[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $3}'))
	mf[$id]=$val
	mfavg=${mf[$id]}
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $4}'))
	ms[$id]=$val
	msavg=$(($msavg+${ms[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $5}'))
	mb[$id]=$val
	mbavg=$(($mbavg+${mb[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $6}'))
	ma[$id]=$val
	maavg=$(($maavg+${mb[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $7}'))
	st[$id]=$val
	stavg=$(($stavg+${st[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $8}'))
	su[$id]=$val
	suavg=$((${su[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $9}'))
	sf[$id]=$val
	sfavg=$(($sfavg+${sf[$id]}))
	path=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $10}'))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="[M,]" 'NR!=1 {print $11}'))
	path_size[$id]=$val
	psavg=$(($psavg+${path_size[$id]}))
	((id++))
done
```
Berikutnya, akan dilakukan proses sorting dari masing-masing indikator yang diperlukan untuk mem & swap. Tujuannya adalah untuk dapat menghitung nilai maksimum maupun nilai minimum dari setiap indikator tersebut.
```
mt_sorted=($(for i in "${mt[@]}"; do echo "$i"; done | sort -n))
mu_sorted=($(for i in "${mu[@]}"; do echo "$i"; done | sort -n))
mf_sorted=($(for i in "${mf[@]}"; do echo "$i"; done | sort -n))
ms_sorted=($(for i in "${ms[@]}"; do echo "$i"; done | sort -n))
mb_sorted=($(for i in "${mb[@]}"; do echo "$i"; done | sort -n))
ma_sorted=($(for i in "${ma[@]}"; do echo "$i"; done | sort -n))
st_sorted=($(for i in "${st[@]}"; do echo "$i"; done | sort -n))
su_sorted=($(for i in "${su[@]}"; do echo "$i"; done | sort -n))
sf_sorted=($(for i in "${sf[@]}"; do echo "$i"; done | sort -n))
path_size_sorted=($(for i in "${path_size[@]}"; do echo "$i"; done | sort -n))
```
Lalu, untuk perhitungan pada nilai average (rerata), dilakukan pembagian dari nilai yang didapatkan dari awal loop tadi, menggunakan rumus mean/avg --> total/jumlah data --> contoh: total=mtavg, jumlah data=id --> mt_avg = mtavg/id. Kemudian, akan dilakukan decrement pada id untuk mempermudah pemanggilan array berikutnya karena array memiliki space dari 0, 1, 2, ..., (id-1).
```
mt_avg=$((mtavg/id))
mu_avg=$((muavg/id))
mf_avg=$((mfavg/id))
ms_avg=$((msavg/id))
mb_avg=$((mbavg/id))
ma_avg=$((maavg/id))
st_avg=$((stavg/id))
su_avg=$((suavg/id))
sf_avg=$((sfavg/id))
path_size_avg=$((psavg/id))
((id--))
```
Terakhir, semua variabel dan proses yang sudah diciptakan sebelumnya tinggal dicetak ke dalam file agregasi log yang dibuat sesuai format pada soal.
```
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "/home/raul/log/$d/metrics_agg_$d.log"
echo "minimum,${mt_sorted[0]},${mu_sorted[0]},${mf_sorted[0]},${ms_sorted[0]},${mb_sorted[0]},${ma_sorted[0]},${st_sorted[0]},${su_sorted[0]},${sf_sorted[9]},$path,${path_size_sorted[0]}M" >> "/home/raul/log/$d/metrics_agg_$d.log"
echo "maximum,${mt_sorted[$id]},${mu_sorted[$id]},${mf_sorted[$id]},${ms_sorted[$id]},${mb_sorted[$id]},${ma_sorted[$id]},${st_sorted[$id]},${su_sorted[$id]},${sf_sorted[$id]},$path,${path_size_sorted[$id]}M" >> "/home/raul/log/$d/metrics_agg_$d.log"
echo "average,$mt_avg,$mu_avg,$mf_avg,$ms_avg,$mb_avg,$ma_avg,$st_avg,$su_avg,$sf_avg,$path,$path_size_avg""M" >> "/home/raul/log/$d/metrics_agg_$d.log"
```
Lalu, karena file metrics agregasi berjalan setiap jamnya, maka dengan cara yang sama pada permasalahan sebelumnya, akan dilakukan konfigurasi crontab pada terminal:
```
crontab -e
```
Kemudian, diisi dengan command cron sebagai berikut untuk menjalankan file aggregate_minutes_to_hourly_log.sh setiap jamnya.
```
0 * * * * /home/raul/aggregate_minutes_to_hourly_log.sh
```

# Semua File Log Hanya dapat Dibaca oleh User Pemilik File
Tidak lupa juga, menggunakan chown untuk mengubah owner menjadi nama user sekarang (dalam konteks ini, "raul") beserta dengan lokasi direktori dari file log agregasi tersebut (/home/raul/log/$d/metrics_agg_$d.log) untuk memenuhi kriteria soal D.
Pada aggregate_minutes_to_hourly_log.sh:
```
chown "raul" "/home/raul/log/$d/metrics_agg_$d.log"
```
Sedangkan, untuk minute_log.sh:
```
chown "raul" "/home/raul/log/$fd"
```

# Dokumentasi Program No. 3
Pada no. 3a akan terbuat folder sebagai berikut:
![image-13.png](https://drive.google.com/uc?export=view&id=1xCpxqqABXKG7TJCwCSg1I4Eqyl_-Mx8M)

Kemudian, di dalamnya akan terbuat file log sebagai berikut:
![image-14.png](https://drive.google.com/uc?export=view&id=1Zr093DEfbzIGyBVaLd9hefrIfH2hcdFe)

Di dalam setiap file log-nya, akan diisi dengan format sebagai berikut:
![image-15.png](https://drive.google.com/uc?export=view&id=1q2raqQQpJIGAMwINdnqu1tS2FQcgMcUR)

Lalu, dalam file agregasi setiap jam memiliki isi sebagai berikut:
![image-16.png](https://drive.google.com/uc?export=view&id=176bL8lVbYlDHOsi6Ph_rIlhg_ADbgyoT)

Sedangkan, konfigurasi cron untuk menjalankan file minute_log.sh setiap menit dan file aggregate_minutes_to_hourly_log.sh setiap jamnya menggunakan command "crontab -e" adalah sebagai berikut:
![image-16.png](https://drive.google.com/uc?export=view&id=1lic_QRKgl8pMWDbNgHSM3U9N7-3b2NvF)
