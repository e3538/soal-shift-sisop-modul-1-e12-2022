#!/bin/bash
d="$(date -d '1 hour ago' +'%Y%m%d%H')"
if ! [[ -d /home/raul/log/$d ]]
	then
    	mkdir /home/raul/log/$d
fi
files=($(awk '{print}' <<< `ls -r "/home/raul/log/$d"`))

mtavg=0
muavg=0
mfavg=0
msavg=0
mbavg=0
maavg=0
stavg=0
suavg=0
sfavg=0
psavg=0
id=0

for i in ${files[@]}
do
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $1}'))
	mt[$id]=$val
	mtavg=$(($mtavg+${mt[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $2}'))
	mu[$id]=$val
	muavg=$(($muavg+${mu[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $3}'))
	mf[$id]=$val
	mfavg=${mf[$id]}
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $4}'))
	ms[$id]=$val
	msavg=$(($msavg+${ms[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $5}'))
	mb[$id]=$val
	mbavg=$(($mbavg+${mb[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $6}'))
	ma[$id]=$val
	maavg=$(($maavg+${mb[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $7}'))
	st[$id]=$val
	stavg=$(($stavg+${st[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $8}'))
	su[$id]=$val
	suavg=$((${su[$id]}))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $9}'))
	sf[$id]=$val
	sfavg=$(($sfavg+${sf[$id]}))
	path=($(cat "/home/raul/log/$d/$i" | awk -v FS="," 'NR!=1 {print $10}'))
	val=($(cat "/home/raul/log/$d/$i" | awk -v FS="[M,]" 'NR!=1 {print $11}'))
	path_size[$id]=$val
	psavg=$(($psavg+${path_size[$id]}))
	((id++))
done

mt_sorted=($(for i in "${mt[@]}"; do echo "$i"; done | sort -n))
mu_sorted=($(for i in "${mu[@]}"; do echo "$i"; done | sort -n))
mf_sorted=($(for i in "${mf[@]}"; do echo "$i"; done | sort -n))
ms_sorted=($(for i in "${ms[@]}"; do echo "$i"; done | sort -n))
mb_sorted=($(for i in "${mb[@]}"; do echo "$i"; done | sort -n))
ma_sorted=($(for i in "${ma[@]}"; do echo "$i"; done | sort -n))
st_sorted=($(for i in "${st[@]}"; do echo "$i"; done | sort -n))
su_sorted=($(for i in "${su[@]}"; do echo "$i"; done | sort -n))
sf_sorted=($(for i in "${sf[@]}"; do echo "$i"; done | sort -n))
path_size_sorted=($(for i in "${path_size[@]}"; do echo "$i"; done | sort -n))

mt_avg=$((mtavg/id))
mu_avg=$((muavg/id))
mf_avg=$((mfavg/id))
ms_avg=$((msavg/id))
mb_avg=$((mbavg/id))
ma_avg=$((maavg/id))
st_avg=$((stavg/id))
su_avg=$((suavg/id))
sf_avg=$((sfavg/id))
path_size_avg=$((psavg/id))
((id--))
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "/home/raul/log/$d/metrics_agg_$d.log"
echo "minimum,${mt_sorted[0]},${mu_sorted[0]},${mf_sorted[0]},${ms_sorted[0]},${mb_sorted[0]},${ma_sorted[0]},${st_sorted[0]},${su_sorted[0]},${sf_sorted[9]},$path,${path_size_sorted[0]}M" >> "/home/raul/log/$d/metrics_agg_$d.log"
echo "maximum,${mt_sorted[$id]},${mu_sorted[$id]},${mf_sorted[$id]},${ms_sorted[$id]},${mb_sorted[$id]},${ma_sorted[$id]},${st_sorted[$id]},${su_sorted[$id]},${sf_sorted[$id]},$path,${path_size_sorted[$id]}M" >> "/home/raul/log/$d/metrics_agg_$d.log"
echo "average,$mt_avg,$mu_avg,$mf_avg,$ms_avg,$mb_avg,$ma_avg,$st_avg,$su_avg,$sf_avg,$path,$path_size_avg""M" >> "/home/raul/log/$d/metrics_agg_$d.log"

chown "raul" "/home/raul/log/$d/metrics_agg_$d.log"
