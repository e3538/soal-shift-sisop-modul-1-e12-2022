#!/bin/bash

fd="$(date -d '1 hour ago' +'%Y%m%d%H')"
d=$(date +"%Y%m%d%H%M%S")

loc=/home/raul/log/$fd

if [[ ! -d "$loc" ]]
then
    	mkdir $loc
fi

echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$loc/metrics_$d.log"
free -m /home/raul | awk '/[0-9]+/{for (i=2; i<NF; i++) printf $i ","; print $NF}' ORS="," >> "$loc/metrics_$d.log"
du -sh /home/raul | awk '/[0-9]+/{print $2","$1}' >> "$loc/metrics_$d.log"

# crontab b: * * * * * /home/raul/minute_log.sh
# crontab c: 0 * * * * /home/raul/aggregate_minutes_to_hourly_log.sh
# d: chown "raul" "/home/raul/log/$fd"
